/*
 * Main.cpp
 *
 *  Created on: 25 de mar. de 2016
 *      Author: agustin
 */

#include "Main.h"
#include "Arduino.h"
#include "config.h"

void Main::setup(){
	//Serial.begin(9600);
	Serial1.begin(9600);

  	POSICIONES[0] = 0;
  	POSICIONES[1] = 0;
  	POSICIONES[2] = 0;
  	POSICIONES[3] = 0;

  	VELOCIDADES[0] = 15000;
  	VELOCIDADES[1] = 15000;
  	VELOCIDADES[2] = 15000;
  	VELOCIDADES[3] = 15000;
  
  	m0 = AccelStepper(AccelStepper::DRIVER,X_STEP_PIN,X_DIR_PIN);
	m1 = AccelStepper(AccelStepper::DRIVER,Y_STEP_PIN,Y_DIR_PIN);
	m2 = AccelStepper(AccelStepper::DRIVER,Z_STEP_PIN,Z_DIR_PIN);
	m3 = AccelStepper(AccelStepper::DRIVER,E_STEP_PIN,E_DIR_PIN);

  	m0.setPinsInverted(false,false,true);
  	m1.setPinsInverted(false,false,true);
  	m2.setPinsInverted(false,false,true);
  	m3.setPinsInverted(false,false,true);
 
  	m0.setEnablePin(X_ENABLE_PIN);
  	m1.setEnablePin(Y_ENABLE_PIN);
  	m2.setEnablePin(Z_ENABLE_PIN);
  	m3.setEnablePin(E_ENABLE_PIN);

  	digitalWrite(X_ENABLE_PIN,HIGH);
	digitalWrite(Y_ENABLE_PIN,HIGH);
	digitalWrite(Z_ENABLE_PIN,HIGH);
	digitalWrite(E_ENABLE_PIN,HIGH);

  	mx.addStepper(m0);
  	mx.addStepper(m1);
  	mx.addStepper(m2);
  	mx.addStepper(m3);

}

void Main::main(){

	int i = 0;

	START_FLAG = false;

	for(;;){
		if(Serial1.available() >= SIZE){
			for(i=0; i<SIZE; i++){
				COMANDO[i] = Serial1.read();
			}
			this->Interprete();
		}

		if(START_FLAG){

			digitalWrite(X_ENABLE_PIN,LOW);
			digitalWrite(Y_ENABLE_PIN,LOW);
			digitalWrite(Z_ENABLE_PIN,LOW);
			digitalWrite(E_ENABLE_PIN,LOW);

			m0.setMaxSpeed(VELOCIDADES[0]);
    		m1.setMaxSpeed(VELOCIDADES[1]);
    		m2.setMaxSpeed(VELOCIDADES[2]);
    		m3.setMaxSpeed(VELOCIDADES[3]);

		}

		while(START_FLAG){
    		mx.moveTo(POSICIONES);
	  		mx.runSpeedToPosition();
	  		if(m0.distanceToGo() == 0 && m1.distanceToGo() == 0 && m2.distanceToGo() == 0 && m3.distanceToGo() == 0){
	  			START_FLAG = false;
	  			
	  			digitalWrite(X_ENABLE_PIN,HIGH);
				digitalWrite(Y_ENABLE_PIN,HIGH);
				digitalWrite(Z_ENABLE_PIN,HIGH);
				digitalWrite(E_ENABLE_PIN,HIGH);

	  		}
	  	}
	}

}

unsigned char Main::Interprete(){

	switch(COMANDO[0]){
		case CMD_M_WRITE:
			VELOCIDADES[COMANDO[1]-1] = getInt(VELOCIDAD_ID,COMANDO);
			POSICIONES[COMANDO[1]-1] = getInt(PASOS_ID,COMANDO);
			break;

		case CMD_M_READ:
			this->toByte(VELOCIDADES[COMANDO[1]-1],VELOCIDAD_ID,COMANDO);
			this->toByte(POSICIONES[COMANDO[1]-1],PASOS_ID,COMANDO);
			sendComando();
			break;

		case CMD_START:
			this->START_FLAG = true;
			break;

		case CMD_M_TEST:

			break;

	}

	return COMANDO[0];
}

int32_t Main::getInt(int byte_start, unsigned char *data){
	int32_t a = 0;

	a = data[byte_start] << 8;
	a = (a | data[byte_start+1]) << 8;
	a = (a | data[byte_start+2]) << 8;
	a = (a | data[byte_start+3]);

	return a;
}

void Main::toByte(int32_t intvar,int byte_start, unsigned char *data){

	data[byte_start + 3] = (intvar & 0xff);
	data[byte_start + 2] = (intvar & 0xff00) >> 8;
	data[byte_start + 1] = (intvar & 0xff0000) >> 16;
	data[byte_start] = (intvar & 0xff000000) >> 24;

}

void Main::sendComando(){
	int i = 0;

	for(i=0;i<SIZE;i++){
		Serial1.write(COMANDO[i]);
	}

}
