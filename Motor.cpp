/*
 * Motor.cpp
 *
 *  Created on: 25 de mar. de 2016
 *      Author: agustin
 */

#include "Motor.h"

void Motor::setPin(unsigned char enable_pin, unsigned char step_pin, unsigned char dir_pin){
	this->POSICION = 0;
	this->VELOCIDAD = 0;
	this->PASOS_BUFF = 0;
	this->DELAY_BUFF = this->VELOCIDAD;
	this->ENABLE_PIN = enable_pin;
	this->DIR_PIN = dir_pin;
	this->STEP_PIN = step_pin;

	pinMode(ENABLE_PIN,OUTPUT);
	pinMode(STEP_PIN,OUTPUT);
	pinMode(DIR_PIN,OUTPUT);

	digitalWrite(ENABLE_PIN,HIGH);
	digitalWrite(STEP_PIN,LOW);
	digitalWrite(DIR_PIN,HIGH);
}

int32_t Motor::getPosicion(){
	return this->POSICION;
}

int32_t Motor::getVelocidad(){
	return this->VELOCIDAD;
}

void Motor::setVelocidad(int32_t velocidad){
	this->VELOCIDAD = velocidad;
}

void Motor::setStep(int32_t pasos){
	if(PASOS_BUFF == 0){
		PASOS_BUFF = pasos;
	}
}

void Motor::run(){
	if(this->DELAY_BUFF == 0){
		this->DELAY_BUFF = this->VELOCIDAD;
		this->step();
	}else{
		DELAY_BUFF--;
	}
}

void Motor::step(){
	digitalWrite(ENABLE_PIN,LOW);
	if(PASOS_BUFF != 0){
		if(PASOS_BUFF > 0){
			this->moveStep(true);
			PASOS_BUFF--;
		}else{
			this->moveStep(false);
			PASOS_BUFF++;
		}
	}
	digitalWrite(ENABLE_PIN,HIGH);
}

void Motor::moveStep(boolean dir){
	if(dir){
		digitalWrite(DIR_PIN,HIGH);
		this->POSICION++;
	}else{
		digitalWrite(DIR_PIN,LOW);
		this->POSICION--;
	}

	digitalWrite(STEP_PIN,HIGH);
	digitalWrite(STEP_PIN,LOW);

}


