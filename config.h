#ifndef CONFIG_H_
#define CONFIG_H_

#define SIZE 				14

#define CMD_M_READ			0x31
#define CMD_M_WRITE 		0x30
#define CMD_M_TEST 			0x32
#define CMD_START			0x80

#define VELOCIDAD_ID 		2
#define PASOS_ID 			6
#define NMOTOR_ID 			1
#define TIME_ID 			10

#define X_STEP_PIN         	54
#define X_DIR_PIN          	55
#define X_ENABLE_PIN       	38

#define Y_STEP_PIN         	60
#define Y_DIR_PIN          	61
#define Y_ENABLE_PIN       	56

#define Z_STEP_PIN         	46
#define Z_DIR_PIN          	48
#define Z_ENABLE_PIN       	62

#define E_STEP_PIN         	26
#define E_DIR_PIN          	28
#define E_ENABLE_PIN       	24

#endif
