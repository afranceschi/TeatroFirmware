/*
 * Motor.h
 *
 *  Created on: 25 de mar. de 2016
 *      Author: agustin
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "Arduino.h"

class Motor {
	public:
		void setPin(unsigned char enable_pin, unsigned char step_pin, unsigned char dir_pin);
		int32_t getPosicion();
		void setStep(int32_t pasos);
		int32_t getVelocidad();
		void setVelocidad(int32_t velocidad);
		void run();

	private:
		int32_t POSICION;
		int32_t VELOCIDAD;
		int32_t PASOS_BUFF;
		int32_t DELAY_BUFF;

		unsigned char STEP_PIN;
		unsigned char ENABLE_PIN;
		unsigned char DIR_PIN;

		void moveStep(boolean dir);
		void step();
};

#endif /* MOTOR_H_ */
