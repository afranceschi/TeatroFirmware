/*
 * Main.h
 *
 *  Created on: 25 de mar. de 2016
 *      Author: agustin
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "config.h"
#include "Arduino.h"
#include "AccelStepper.h"
#include "MultiStepper.h"
 

class Main {
	public:
		void setup();
		void main();
		unsigned char Interprete();
		int32_t getInt(int byte_start, unsigned char *data);
		void toByte(int32_t intvar,int byte_start, unsigned char *data);
		void sendComando();

	private:
		unsigned char COMANDO[SIZE];
		bool START_FLAG;
    	long POSICIONES[4];
    	long VELOCIDADES[4];
		MultiStepper mx;

    	AccelStepper m0;
    	AccelStepper m1;
    	AccelStepper m2;
    	AccelStepper m3;
};

#endif /* MAIN_H_ */
